package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PesertaMapper;
import com.example.model.PesertaModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PesertaServiceDatabase implements PesertaService
{
    @Autowired
    private PesertaMapper pesertaMapper;

    @Override
    public List<PesertaModel> selectAllPesertas (String kodeProdi)
    {
        log.info ("select all peserta with kodeProdi {}", kodeProdi);
        return pesertaMapper.selectAllPesertas (kodeProdi);
    }
    
    @Override
    public PesertaModel selectPeserta (String nomor)
    {
        log.info ("select peserta with nomor {}", nomor);
        return pesertaMapper.selectPeserta (nomor);
    }
    
    @Override
    public Integer getUmur (String nomor)
    {
        log.info ("getUmur peserta with nomor {}", nomor);
        return pesertaMapper.getUmur(nomor); 
    }
    
    @Override
    public String getProdi (String nomor)
    {
        log.info ("getProdi peserta with nomor {}", nomor);
        return pesertaMapper.getProdi(nomor); 
    }
    
    @Override
    public String getUrlUniv (String nomor)
    {
        log.info ("getUrlUniv peserta with nomor {}", nomor);
        return pesertaMapper.getUrlUniv(nomor); 
    }
    
    public PesertaModel getTglLahirMax (String kodeProdi)
    {
        log.info ("getTglLahirMax with kodeProdi {}", kodeProdi);
        return pesertaMapper.getTglLahirMax (kodeProdi);
    }
    
    public PesertaModel getTglLahirMin (String kodeProdi)
    {
        log.info ("getTglLahirMin with kodeProdi {}", kodeProdi);
        return pesertaMapper.getTglLahirMin (kodeProdi);
    }
}
