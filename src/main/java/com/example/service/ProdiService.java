package com.example.service;

import java.util.List;

import com.example.model.ProdiModel;

public interface ProdiService
{

    List<ProdiModel> selectAllProdis (String kodeUniv);
    ProdiModel selectProdi (String kodeUniv);   
    ProdiModel selectProdiUniversitas (String kodeUniv);   
    String getUniv (String kodeProdi);

}
