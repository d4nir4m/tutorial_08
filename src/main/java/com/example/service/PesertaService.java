package com.example.service;

import java.util.List;

import com.example.model.PesertaModel;

public interface PesertaService
{

    List<PesertaModel> selectAllPesertas (String kodeProdi);
    PesertaModel selectPeserta (String nomor);
    Integer getUmur (String nomor);
    String getProdi (String nomor);
    String getUrlUniv (String nomor);
    PesertaModel getTglLahirMax (String kodeProdi);
    PesertaModel getTglLahirMin (String kodeProdi);

}
