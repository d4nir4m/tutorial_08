package com.example.service;

import java.util.List;

import com.example.model.UnivModel;

public interface UnivService
{

    List<UnivModel> selectAllUnivs ();
    UnivModel selectUniv (String kodeUniv);
  

}
