package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UnivMapper;
import com.example.model.UnivModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UnivServiceDatabase implements UnivService
{
    @Autowired
    private UnivMapper univMapper;

    @Override
    public List<UnivModel> selectAllUnivs ()
    {
        log.info ("select all univ");
        return univMapper.selectAllUnivs ();
    }
    
    @Override
    public UnivModel selectUniv (String kodeUniv)
    {
        log.info ("select univ with kodeUniv {}", kodeUniv);
        return univMapper.selectUniv (kodeUniv);
    }
    
  
}
