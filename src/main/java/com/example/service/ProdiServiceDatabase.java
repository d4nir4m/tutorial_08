package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.dao.ProdiMapper;
import com.example.model.ProdiModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService
{
    @Autowired
    private ProdiMapper prodiMapper;

    @Override
    public List<ProdiModel> selectAllProdis (String kodeUniv)
    {
        log.info ("select all prodi with kodeUniv {}",  kodeUniv);
        return prodiMapper.selectAllProdis (kodeUniv);
    }    

    @Override
    public ProdiModel selectProdi (String kodeProdi)
    {
        log.info ("select prodi with kodeProdi {}", kodeProdi);
        return prodiMapper.selectProdi (kodeProdi);
    }
    
    @Override
    public ProdiModel selectProdiUniversitas (String kodeProdi)
    {
        log.info ("select prodiUniversitas with kodeProdi {}", kodeProdi);
        return prodiMapper.selectProdiUniversitas (kodeProdi);
    }

    @Override
    public String getUniv (String kodeProdi)
    {
        log.info ("getUniv with kodeProdi {}", kodeProdi);
        return prodiMapper.getUniv(kodeProdi); 
    }
    
}
