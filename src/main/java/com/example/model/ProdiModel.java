package com.example.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProdiModel {
	private String kodeUniv;
    private String kodeProdi;
    private String namaProdi;
	private List<ProdiModel> prodis;	
}



