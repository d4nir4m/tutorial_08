package com.example.model;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PesertaModel {
	private String nomor;
	private String nama;
	private Date tglLahir;
	private String kodeProdi;
	private List<PesertaModel> pesertas;	
}
