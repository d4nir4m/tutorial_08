package com.example.dao;

import java.util.List;

//import org.apache.ibatis.annotations.Delete;
//import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
//import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
//import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
//import org.apache.ibatis.annotations.Many;
//import org.apache.ibatis.annotations.One;

import com.example.model.UnivModel;


@Mapper
public interface UnivMapper
{
	
  
    @Select("select kode_univ, nama_univ, url_univ from univ")
    @Results(value = {
    	    @Result(property="kodeUniv", column="kode_univ"),
    	    @Result(property="namaUniv", column="nama_univ"),
    	    @Result(property="urlUniv", column="url_univ")
        })
    List<UnivModel> selectAllUnivs ();

    @Select("select kode_univ, nama_univ, url_univ from univ where kode_univ = #{kodeUniv}")
    @Results(value = {
    	    @Result(property="kodeUniv", column="kode_univ"),
    	    @Result(property="namaUniv", column="nama_univ"),
    	    @Result(property="urlUniv", column="url_univ")
    })
    UnivModel selectUniv (@Param("kodeUniv") String kodeUniv);    
    

}
