package com.example.dao;

import java.util.List;

//import org.apache.ibatis.annotations.Delete;
//import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
//import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
//import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.One;

import com.example.model.ProdiModel;


@Mapper
public interface ProdiMapper
{
	
    @Select("select kode_univ, kode_prodi, nama_prodi from prodi where kode_univ = #{kodeUniv}")
    @Results(value = {
	    @Result(property="kodeUniv", column="kode_univ"),
	    @Result(property="kodeProdi", column="kode_prodi"),
	    @Result(property="namaProdi", column="nama_prodi")	 
        })
    List<ProdiModel> selectAllProdis (@Param("kodeUniv") String kodeUniv);  ;
	
    @Select("select kode_univ, kode_prodi, nama_prodi from prodi where kode_prodi = #{kodeProdi}")
    @Results(value = {
	    @Result(property="kodeUniv", column="kode_univ"),
	    @Result(property="kodeProdi", column="kode_prodi"),
	    @Result(property="namaProdi", column="nama_prodi")	   
    })
    ProdiModel selectProdi (@Param("kodeProdi") String kodeProdi);     
    

    @Select("select kode_univ, kode_prodi, nama_prodi from prodi where kode_prodi = #{kodeProdi}")
    @Results(value = {
    	@Result(property="kodeUniv", column="kode_univ"),
	    @Result(property="nama", column="nama"),
	    @Result(property="tglLahir", column="tgl_lahir"),
	    @Result(property="prodi", column="nomor",
	    javaType = List.class,
	    one=@One(select="selectUniversitas"))
    })
    ProdiModel selectProdiUniversitas (@Param("kodeProdi") String kodeProdi);   
    
    @Select("select kode_univ from prodi where kode_prodi = #{kodeProdi}")
    @Results(value = {
	    @Result(property="kodeUniv", column="kode_univ")
    })
    String getUniv (@Param("kodeProdi") String kodeProdi);
    
}
