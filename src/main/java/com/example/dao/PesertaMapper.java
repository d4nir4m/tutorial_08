package com.example.dao;

import java.util.List;

//import org.apache.ibatis.annotations.Delete;
//import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
//import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
//import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.One;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;


@Mapper
public interface PesertaMapper
{
	
  
    @Select("select nomor, nama, tgl_lahir, kode_prodi from peserta"
    		+ " where kode_prodi is not null and kode_prodi = #{kodeProdi}")
    @Results(value = {
    	    @Result(property="nomor", column="nomor"),
    	    @Result(property="nama", column="nama"),
    	    @Result(property="tglLahir", column="tgl_lahir"),
    	    @Result(property="kodeProdi", column="kode_prodi")
        })
    List<PesertaModel> selectAllPesertas (@Param("kodeProdi") String kodeProdi);
    
    @Select("select nomor, nama, tgl_lahir, kode_prodi from peserta where nomor = #{nomor}")
    @Results(value = {
	    @Result(property="nomor", column="nomor"),
	    @Result(property="nama", column="nama"),
	    @Result(property="tglLahir", column="tgl_lahir"),
	    @Result(property="kodeProdi", column="kode_prodi")
    })
    PesertaModel selectPeserta (@Param("nomor") String nomor);     
    
    @Select("select timestampdiff(year, (select tgl_lahir from peserta where nomor = #{nomor}), curdate()) as umur")
    @Results(value = {
	    @Result(property="umur", column="umur")	    
    })
    Integer getUmur (@Param("nomor") String nomor);
    
    @Select("select nomor, nama, tgl_lahir, kode_prodi from peserta where kode_prodi is not null and kode_prodi = #{kodeProdi} "
    		+ "and tgl_lahir = (select max(tgl_lahir) from peserta where kode_prodi is not null and kode_prodi = #{kodeProdi})")
    @Results(value = {
		@Result(property="nomor", column="nomor"),
 	    @Result(property="nama", column="nama"),
 	    @Result(property="tglLahir", column="tgl_lahir"),
 	    @Result(property="kodeProdi", column="kode_prodi")   
    })
    PesertaModel getTglLahirMax (@Param("kodeProdi") String kodeProdi);
    
    @Select("select nomor, nama, tgl_lahir, kode_prodi from peserta where kode_prodi is not null and kode_prodi = #{kodeProdi} "
    		+ "and tgl_lahir = (select min(tgl_lahir) from peserta where kode_prodi is not null and kode_prodi = #{kodeProdi})")
    @Results(value = {
		@Result(property="nomor", column="nomor"),
 	    @Result(property="nama", column="nama"),
 	    @Result(property="tglLahir", column="tgl_lahir"),
 	    @Result(property="kodeProdi", column="kode_prodi")   
    })
    PesertaModel getTglLahirMin (@Param("kodeProdi") String kodeProdi);
    
    @Select("select nomor, nama, tgl_lahir, kode_prodi from peserta where nomor = #{nomor}")
    @Results(value = {
    	@Result(property="nomor", column="nomor"),
	    @Result(property="nama", column="nama"),
	    @Result(property="tglLahir", column="tgl_lahir"),
	    @Result(property="prodi", column="kode_prodi",
	    javaType = List.class,
	    one=@One(select="selectProdiUniv"))
    })
    PesertaModel selectPesertaProdi (@Param("kodeProdi") String kodeProdi);   
    
    @Select("select distinct prodi.kode_univ, prodi.kode_prodi, prodi.nama_prodi" +
    		"from prodi join peserta" +
    		"on prodi.kode_prodi = peserta.kode_prodi" +
    		"where prodi.kode_prodi = #{kodeProdi}")
    List<ProdiModel> selectProdiUniv (@Param("kodeProdi") String kodeProdi);


    @Select("select  CONCAT(a.nama_prodi,' ', c.nama_univ) as nama_prodi " +
			"from prodi a, peserta b, univ c " + 
			"where a.kode_prodi = b.kode_prodi " +
			"and a.kode_univ = c.kode_univ " +
			"and b.nomor = #{nomor}")
    @Results(value = {
	    @Result(property="nama_prodi", column="nama_prodi")
    })
    String getProdi (@Param("nomor") String nomor);
    
    @Select("select c.url_univ " +
			"from prodi a, peserta b, univ c " + 
			"where a.kode_prodi = b.kode_prodi " +
			"and a.kode_univ = c.kode_univ " +
			"and b.nomor = #{nomor}")
    @Results(value = {
	    @Result(property="url_univ", column="url_univ")
    })
    String getUrlUniv (@Param("nomor") String nomor);

}
