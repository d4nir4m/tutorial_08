package com.example.controller;

import java.util.List;
//import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.UnivModel;
import com.example.service.UnivService;
import com.example.model.ProdiModel;
import com.example.service.ProdiService;



@Controller
public class UnivController
{
    @Autowired
    UnivService univDAO;
    
    @Autowired
    ProdiService prodiDAO;

    
    @RequestMapping("/univ")
    public String viewAll (Model model)
    {
        List<UnivModel> univs = univDAO.selectAllUnivs ();
        model.addAttribute ("univs", univs);

        return "universitas_list";
    }
    
    @RequestMapping("/univ/{kodeUniv}")
    public String viewDetail(Model model, @PathVariable String kodeUniv)
    {
    	UnivModel univ = univDAO.selectUniv(kodeUniv);
        
    	if (univ != null) {        
            model.addAttribute ("univ", univ);
            
            List<ProdiModel> prodis = prodiDAO.selectAllProdis(kodeUniv);            
            model.addAttribute("prodis", prodis);
            return "universitas_detail";
        } else {
        	model.addAttribute("kodeUniv", kodeUniv);
            return "universitas_not-found";
        }
    }    
    
}
