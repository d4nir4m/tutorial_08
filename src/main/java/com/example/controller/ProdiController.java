package com.example.controller;

import java.util.List;
//import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.ProdiModel;
import com.example.model.UnivModel;
import com.example.model.PesertaModel;
import com.example.service.ProdiService;
import com.example.service.UnivService;
import com.example.service.PesertaService;



@Controller
public class ProdiController
{
    @Autowired
    ProdiService prodiDAO;
    
    @Autowired
    UnivService univDAO;
    
    @Autowired
    PesertaService pesertaDAO;

    @RequestMapping(value = "/prodi", method = RequestMethod.GET)
    public String viewProdi(Model model,
            @RequestParam(value = "kode", required = true) String kodeProdi) 
    {
        
    	ProdiModel prodi = prodiDAO.selectProdi(kodeProdi);
        
    	if (prodi != null) {        
            model.addAttribute ("prodi", prodi);    
            
         	String kode_univ = prodiDAO.getUniv(kodeProdi);        	
            UnivModel univ = univDAO.selectUniv(kode_univ);
            model.addAttribute ("univ", univ);
            
            List<PesertaModel> pesertas = pesertaDAO.selectAllPesertas(kodeProdi);
            model.addAttribute ("pesertas", pesertas);
            
            PesertaModel pesertaMax = pesertaDAO.getTglLahirMax(kodeProdi);
            model.addAttribute ("pesertaMax", pesertaMax);
            
            PesertaModel pesertaMin = pesertaDAO.getTglLahirMin(kodeProdi);
            model.addAttribute ("pesertaMin", pesertaMin);       

            Integer umurMax = pesertaDAO.getUmur(pesertaMax.getNomor());
            model.addAttribute ("umurMax", umurMax);
            
            Integer umurMin = pesertaDAO.getUmur(pesertaMin.getNomor());
            model.addAttribute ("umurMin", umurMin);
            
            return "prodi_detail";
        } else {
        	model.addAttribute("kodeProdi", kodeProdi);
            return "prodi_not-found";
        }
    }    
    
}
