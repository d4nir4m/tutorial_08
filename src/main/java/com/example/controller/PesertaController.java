package com.example.controller;

//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.service.PesertaService;
//import com.example.service.PesertaServiceDatabase;



@Controller
public class PesertaController
{
    @Autowired
    PesertaService pesertaDAO;


    @RequestMapping("/")
    public String index ()
    {
        return "index";
    }

    @RequestMapping(value="/pengumuman/submit", method = RequestMethod.POST)
    public String view (Model model,
    					@RequestParam(value = "nomor", required = false) String nomor)
    {
    	
        PesertaModel peserta = pesertaDAO.selectPeserta (nomor);

        if (peserta != null) {
            model.addAttribute ("peserta", peserta);
            
            Integer umur = pesertaDAO.getUmur(nomor);
            model.addAttribute ("umur", umur);
            
            if (peserta.getKodeProdi() != null) {
            	
            	String nama_prodi = pesertaDAO.getProdi(nomor);
            	model.addAttribute ("nama_prodi", nama_prodi);
            	
            	String url_univ = pesertaDAO.getUrlUniv(nomor);
            	model.addAttribute ("url_univ", url_univ);
            	
            	return "pengumuman_submit_lulus";
            } else {            	
            	return "pengumuman_submit_gagal";
            }
            	
        } else {
            model.addAttribute ("nomor", nomor);
            return "pengumuman_submit_not-found";
        }
    }
    
    @RequestMapping(value = "/peserta", method = RequestMethod.GET)
    public String viewProdi(Model model,
            @RequestParam(value = "nomor", required = true) String nomor) 
    {
            	
    	 PesertaModel peserta = pesertaDAO.selectPeserta (nomor);

         if (peserta != null) {
             model.addAttribute ("peserta", peserta);
             
             Integer umur = pesertaDAO.getUmur(nomor);
             model.addAttribute ("umur", umur);
                          	
             return "peserta_detail";         	
         } else {
             model.addAttribute ("nomor", nomor);
             return "pengumuman_submit_not-found";
         }
    }  
}
